class CreateTaxes < ActiveRecord::Migration
  def change
    create_table :taxes do |t|
      t.string :name
      t.float :percent

      t.timestamps null: false
    end
  end
end
