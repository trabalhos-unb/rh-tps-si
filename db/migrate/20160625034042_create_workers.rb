class CreateWorkers < ActiveRecord::Migration
  def change
    create_table :workers do |t|
      t.string :name
      t.string :cpf
      t.string :registry
      t.string :role

      t.timestamps null: false
    end
  end
end
