class AddSectorToRole < ActiveRecord::Migration
  def change
    add_reference :roles, :sector, index: true, foreign_key: true
  end
end
