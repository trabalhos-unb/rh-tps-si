class AddCollumnTasksCompletedToWorkersProfit < ActiveRecord::Migration
  def change
    add_column :workers_profits, :tasks_completed, :integer
  end
end
