class AddSectorToWorker < ActiveRecord::Migration
  def change
    add_reference :workers, :sector, index: true, foreign_key: true
  end
end
