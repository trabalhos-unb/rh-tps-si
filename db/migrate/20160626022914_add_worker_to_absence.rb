class AddWorkerToAbsence < ActiveRecord::Migration
  def change
    add_reference :absences, :worker, index: true, foreign_key: true
  end
end
