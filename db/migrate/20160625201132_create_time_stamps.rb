class CreateTimeStamps < ActiveRecord::Migration
  def change
    create_table :time_stamps do |t|
      t.time :in_hour
      t.time :out_hour

      t.timestamps null: false
    end
  end
end
