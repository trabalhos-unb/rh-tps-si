class RemoveRoleFromWorker < ActiveRecord::Migration
  def change
    remove_column :workers, :role, :string
  end
end
