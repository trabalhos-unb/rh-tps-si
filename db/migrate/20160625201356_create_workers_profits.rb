class CreateWorkersProfits < ActiveRecord::Migration
  def change
    create_table :workers_profits do |t|
      t.float :value

      t.timestamps null: false
    end
  end
end
