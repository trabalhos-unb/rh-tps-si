class CreateAbsences < ActiveRecord::Migration
  def change
    create_table :absences do |t|
      t.string :date

      t.timestamps null: false
    end
  end
end
