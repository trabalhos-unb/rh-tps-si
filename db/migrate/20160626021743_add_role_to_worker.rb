class AddRoleToWorker < ActiveRecord::Migration
  def change
    add_reference :workers, :role, index: true, foreign_key: true
  end
end
