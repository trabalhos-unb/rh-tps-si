class AddCollumnTotalTasksToWorkersProfit < ActiveRecord::Migration
  def change
    add_column :workers_profits, :total_tasks, :integer
  end
end
