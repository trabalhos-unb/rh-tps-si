class AddWorkerToWorkersProfit < ActiveRecord::Migration
  def change
    add_reference :workers_profits, :worker, index: true, foreign_key: true
  end
end
