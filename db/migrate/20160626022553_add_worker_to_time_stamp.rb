class AddWorkerToTimeStamp < ActiveRecord::Migration
  def change
    add_reference :time_stamps, :worker, index: true, foreign_key: true
  end
end
