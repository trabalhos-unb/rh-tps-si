class AddColumnAdmissionDateToWorker < ActiveRecord::Migration
  def change
    add_column :workers, :admission_date, :date
  end
end
