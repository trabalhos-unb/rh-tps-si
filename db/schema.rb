# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160627150049) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "absences", force: :cascade do |t|
    t.string   "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "worker_id"
  end

  add_index "absences", ["worker_id"], name: "index_absences_on_worker_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.float    "salary"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "sector_id"
  end

  add_index "roles", ["sector_id"], name: "index_roles_on_sector_id", using: :btree

  create_table "sectors", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "taxes", force: :cascade do |t|
    t.string   "name"
    t.float    "percent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "time_stamps", force: :cascade do |t|
    t.time     "in_hour"
    t.time     "out_hour"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "worker_id"
  end

  add_index "time_stamps", ["worker_id"], name: "index_time_stamps_on_worker_id", using: :btree

  create_table "workers", force: :cascade do |t|
    t.string   "name"
    t.string   "cpf"
    t.string   "registry"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "role_id"
    t.integer  "sector_id"
    t.date     "admission_date"
  end

  add_index "workers", ["role_id"], name: "index_workers_on_role_id", using: :btree
  add_index "workers", ["sector_id"], name: "index_workers_on_sector_id", using: :btree

  create_table "workers_profits", force: :cascade do |t|
    t.float    "value"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "worker_id"
    t.integer  "tasks_completed"
    t.integer  "total_tasks"
  end

  add_index "workers_profits", ["worker_id"], name: "index_workers_profits_on_worker_id", using: :btree

  add_foreign_key "absences", "workers"
  add_foreign_key "roles", "sectors"
  add_foreign_key "time_stamps", "workers"
  add_foreign_key "workers", "roles"
  add_foreign_key "workers", "sectors"
  add_foreign_key "workers_profits", "workers"
end
