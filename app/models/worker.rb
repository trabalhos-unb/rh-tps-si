class Worker < ActiveRecord::Base
  belongs_to :role
  belongs_to :sector

  has_one :workers_profit
  has_many :absence
  has_many :time_stamp

  validates :name, :registry, :presence => true
  validates :cpf, length: { is: 11 }
  validates :registry, length: { is: 9 }
  validates :name, format: { with: /\A[a-zA-Z]+\z/,
            message: "only allows letters" }
  # add any other characters you'd like to disallow inside the [ brackets ]
  # metacharacters [, \, ^, $, ., |, ?, *, +, (, and ) need to be escaped with a \

end
