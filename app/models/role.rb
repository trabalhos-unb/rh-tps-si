class Role < ActiveRecord::Base
  belongs_to :sector
  has_many :worker

  validates :salary, numericality: true
  validates :name, format: { with: /\A[a-zA-Z]+\z/,
            message: "only allows letters" }
end
