class Tax < ActiveRecord::Base
    validates :percent, numericality: true

    validates :name, format: { with: /\A[a-zA-Z]+\z/,
              message: "only allows letters" }
end
