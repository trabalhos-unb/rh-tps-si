json.array!(@workers) do |worker|
  json.extract! worker, :id, :name, :cpf, :registry, :role
  json.url worker_url(worker, format: :json)
end
