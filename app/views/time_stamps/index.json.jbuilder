json.array!(@time_stamps) do |time_stamp|
  json.extract! time_stamp, :id, :in_hour, :out_hour
  json.url time_stamp_url(time_stamp, format: :json)
end
