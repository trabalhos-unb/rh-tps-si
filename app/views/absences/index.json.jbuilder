json.array!(@absences) do |absence|
  json.extract! absence, :id, :date
  json.url absence_url(absence, format: :json)
end
