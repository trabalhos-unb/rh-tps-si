json.array!(@workers_profits) do |workers_profit|
  json.extract! workers_profit, :id, :value
  json.url workers_profit_url(workers_profit, format: :json)
end
