class TimeStampsController < ApplicationController
  before_action :set_time_stamp, only: [:show, :edit, :update, :destroy]

  # GET /time_stamps
  # GET /time_stamps.json
  def index
    @time_stamps = TimeStamp.all
  end

  # GET /time_stamps/1
  # GET /time_stamps/1.json
  def show
  end

  # GET /time_stamps/new
  def new
    @time_stamp = TimeStamp.new
  end

  # GET /time_stamps/1/edit
  def edit
  end

  # POST /time_stamps
  # POST /time_stamps.json
  def create
    @time_stamp = TimeStamp.new(time_stamp_params)

    respond_to do |format|
      if @time_stamp.save
        format.html { redirect_to @time_stamp, notice: 'Time stamp was successfully created.' }
        format.json { render :show, status: :created, location: @time_stamp }
      else
        format.html { render :new }
        format.json { render json: @time_stamp.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /time_stamps/1
  # PATCH/PUT /time_stamps/1.json
  def update
    respond_to do |format|
      if @time_stamp.update(time_stamp_params)
        format.html { redirect_to @time_stamp, notice: 'Time stamp was successfully updated.' }
        format.json { render :show, status: :ok, location: @time_stamp }
      else
        format.html { render :edit }
        format.json { render json: @time_stamp.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /time_stamps/1
  # DELETE /time_stamps/1.json
  def destroy
    @time_stamp.destroy
    respond_to do |format|
      format.html { redirect_to time_stamps_url, notice: 'Time stamp was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_time_stamp
      @time_stamp = TimeStamp.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def time_stamp_params
      params.require(:time_stamp).permit(:in_hour, :out_hour, :worker_id)
    end
end
