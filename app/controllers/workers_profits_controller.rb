class WorkersProfitsController < ApplicationController
  before_action :set_workers_profit, only: [:show, :edit, :update, :destroy]

  # GET /workers_profits
  # GET /workers_profits.json
  def index
    @workers_profits = WorkersProfit.all
  end

  # GET /workers_profits/1
  # GET /workers_profits/1.json
  def show
  end

  # GET /workers_profits/new
  def new
    @workers_profit = WorkersProfit.new
    @workers = Worker.all.collect {|p| [ p.name, p.id ] }
    @workers.unshift([" "," "])
  end

  # GET /workers_profits/1/edit
  def edit
    @workers = Worker.all.collect {|p| [ p.name, p.id ] }
    @workers.unshift([" "," "])
  end

  # POST /workers_profits
  # POST /workers_profits.json
  def create
    @workers_profit = WorkersProfit.new(workers_profit_params)

    respond_to do |format|
      if @workers_profit.save
        format.html { redirect_to @workers_profit, notice: 'Workers profit was successfully created.' }
        format.json { render :show, status: :created, location: @workers_profit }
      else
        format.html { render :new }
        format.json { render json: @workers_profit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /workers_profits/1
  # PATCH/PUT /workers_profits/1.json
  def update
    respond_to do |format|
      if @workers_profit.update(workers_profit_params)
        format.html { redirect_to @workers_profit, notice: 'Workers profit was successfully updated.' }
        format.json { render :show, status: :ok, location: @workers_profit }
      else
        format.html { render :edit }
        format.json { render json: @workers_profit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /workers_profits/1
  # DELETE /workers_profits/1.json
  def destroy
    @workers_profit.destroy
    respond_to do |format|
      format.html { redirect_to workers_profits_url, notice: 'Workers profit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def worker_statistics
    @worker = Worker.find(params[:id])
    @task_statistics = [['Completed', @worker.workers_profit.tasks_completed],\
    ['Incompleted', @worker.workers_profit.total_tasks - @worker.workers_profit.tasks_completed]]
  end

private
    # Use callbacks to share common setup or constraints between actions.
    def set_workers_profit
      @workers_profit = WorkersProfit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def workers_profit_params
      params.require(:workers_profit).permit(:value, :worker_id, :tasks_completed, :total_tasks)
    end

end
