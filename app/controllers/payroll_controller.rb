class PayrollController < ApplicationController
  def index
    @workers = Worker.all
  end

  def payslip
    @worker = Worker.find(params[:id])
    @taxes = Tax.all
    @payroll = PayRoll.new(@worker.name, @worker.role_id, @worker.admission_date, 1, @worker.absence.count, 0)
    @payroll.base_salary =  Role.find(@payroll.role).salary
    @payroll.role = Role.find(@payroll.role).name

    @total = @payroll.base_salary

    @taxes.each do |tax|
      @total = @total + @total*tax.percent
    end
    @total = @total - (@payroll.base_salary/30)*@payroll.absence

    @payroll.total = @total
  end

  def payroll
    @workers = Worker.all
    @taxes = Tax.all
    @payroll = Array.new

    @workers.each do |worker|
      @temp = PayRoll.new(worker.name, worker.role_id, worker.admission_date, 1, worker.absence.count, 0)
      @temp.base_salary = Role.find(@temp.role).salary
      @temp.role = Role.find(@temp.role).name
      @payroll << @temp
    end

    @payroll.each do |pay|
      @total = pay.base_salary
      @taxes.each do |tax|
        @total = @total + @total*tax.percent
      end
      pay.total = @total
    end
  end

end

private
  PayRoll = Struct.new(:worker, :role, :contracted_at, :base_salary, :absence, :total)
