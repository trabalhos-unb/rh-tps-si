require 'test_helper'

class WorkersProfitsControllerTest < ActionController::TestCase
  setup do
    @workers_profit = workers_profits(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:workers_profits)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create workers_profit" do
    assert_difference('WorkersProfit.count') do
      post :create, workers_profit: { value: @workers_profit.value }
    end

    assert_redirected_to workers_profit_path(assigns(:workers_profit))
  end

  test "should show workers_profit" do
    get :show, id: @workers_profit
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @workers_profit
    assert_response :success
  end

  test "should update workers_profit" do
    patch :update, id: @workers_profit, workers_profit: { value: @workers_profit.value }
    assert_redirected_to workers_profit_path(assigns(:workers_profit))
  end

  test "should destroy workers_profit" do
    assert_difference('WorkersProfit.count', -1) do
      delete :destroy, id: @workers_profit
    end

    assert_redirected_to workers_profits_path
  end
end
