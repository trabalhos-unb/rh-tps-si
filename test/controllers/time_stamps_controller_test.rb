require 'test_helper'

class TimeStampsControllerTest < ActionController::TestCase
  setup do
    @time_stamp = time_stamps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:time_stamps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create time_stamp" do
    assert_difference('TimeStamp.count') do
      post :create, time_stamp: { in_hour: @time_stamp.in_hour, out_hour: @time_stamp.out_hour }
    end

    assert_redirected_to time_stamp_path(assigns(:time_stamp))
  end

  test "should show time_stamp" do
    get :show, id: @time_stamp
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @time_stamp
    assert_response :success
  end

  test "should update time_stamp" do
    patch :update, id: @time_stamp, time_stamp: { in_hour: @time_stamp.in_hour, out_hour: @time_stamp.out_hour }
    assert_redirected_to time_stamp_path(assigns(:time_stamp))
  end

  test "should destroy time_stamp" do
    assert_difference('TimeStamp.count', -1) do
      delete :destroy, id: @time_stamp
    end

    assert_redirected_to time_stamps_path
  end
end
